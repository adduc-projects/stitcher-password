Stitcher Password Encrypter
===

This library encrypts a password for use in the Sticher API.


Example
---

```php
<?php

$deviceId = "abcdef";
$password = "qwerty";

$encrypter = new Adduc\Stitcher\Password();
$encrypted = $encrypter->encrypt($deviceId, $password);

```
