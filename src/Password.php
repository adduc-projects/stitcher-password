<?php

namespace Adduc\Stitcher;

class Password
{
    /**
     * Encrypts password for use in Stitcher's API for authentication.
     * 
     * @param string $deviceId
     * @param string $password
     * @return string
     */
    function encrypt($deviceId, $password)
    {
        $sequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_";

        $passwordLength = strlen($password);
        $deviceIdLength = strlen($deviceId);
        $sequenceLength = strlen($sequence);
        $counter = 0;

        $result = "";

        while ($counter < $passwordLength) {
            $passwordCodePoint = ord($password[$counter]);
            $bits = $passwordCodePoint - ($passwordCodePoint >> 4 << 4);

            $deviceIdPos = ($counter * 2) % $deviceIdLength;
            $sequencePos = (($passwordCodePoint >> 4) + ord($deviceId[$deviceIdPos])) % $sequenceLength;
            $result .= $sequence[$sequencePos];

            $deviceIdPos = (($counter * 2) + 1) % $deviceIdLength;
            $sequencePos = ($bits + ord($deviceId[$deviceIdPos])) % $sequenceLength;
            $result .= $sequence[$sequencePos];

            $counter += 1;
        }

        return $result;
    }

    /**
     * Decrypts password from Stitcher's API
     *
     * @param string $deviceId
     * @param string $encryptedPassword
     * @return string
     */
    public function decrypt($deviceId, $encryptedPassword)
    {
        $sequence = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_";

        $encryptedPasswordLength = strlen($encryptedPassword);
        $deviceIdLength = strlen($deviceId);
        $sequenceLength = strlen($sequence);

        $result = "";

        for ($counter = 0; $counter < $encryptedPasswordLength; $counter+=2) {
            $sequencePos = strpos($sequence, $encryptedPassword[$counter]);
            $deviceIdPos = $deviceId[$counter % $deviceIdLength];
            $piece_1 = ($sequencePos + $sequenceLength - ord($deviceIdPos));
            $piece_1 = ($piece_1 % $sequenceLength) << 4;

            $deviceIdPos = $deviceId[($counter + 1) % $deviceIdLength];
            $sequencePos = strpos($sequence, $encryptedPassword[$counter + 1]);
            $piece_2 = ($sequencePos + $sequenceLength - ord($deviceIdPos)) % 16;

            $result .= chr($piece_1 + $piece_2);
        }

        return $result;
    }
}
