<?php

namespace Adduc\Stitcher;

use PHPUnit\Framework\TestCase;

class PasswordTest extends TestCase
{
    protected $p;
    
    public function setUp()
    {
        $this->p = new Password();
    }

    /**
     * @dataProvider provideEncrypt
     */
    public function testEncrypt($deviceId, $password, $expected)
    {
        $actual = $this->p->encrypt($deviceId, $password);
        $this->assertEquals($expected, $actual);
    }

    public function provideEncrypt()
    {
        return [
            ["abcdef", "qwerty", "ojqrrrokqosv"]
        ];
    }

    /**
     * @dataProvider provideDecrypt
     */
    public function testDecrypt($deviceId, $encrypted, $expected)
    {
        $actual = $this->p->Decrypt($deviceId, $encrypted);
        $this->assertEquals($expected, $actual);
    }

    public function provideDecrypt()
    {
        return [
            ["abcdef", "ojqrrrokqosv", "qwerty"]
        ];
    }

    public function testIdempotent()
    {
        $deviceId = "asdf";
        $password = "zxcv";

        $encrypted = $this->p->encrypt($deviceId, $password);
        $decrypted = $this->p->decrypt($deviceId, $encrypted);

        $this->assertEquals($password, $decrypted);
    }
}
